var gulp = require('gulp'),
    webpack = require('webpack');

gulp.task('scripts', function(callback){
   webpack(require('../../webpack.config.js'), function(err, stats){
        if(err){
            console.log(err.toString());
        }
       console.log(stats.toString());
       callback();
   });
});

/*
webpack(require('../../webpack.config.js'), function(){

This is used to require the webpack config file so that it can find the js
*/
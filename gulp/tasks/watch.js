var gulp = require("gulp"),
    watch = require("gulp-watch"),
    browserSync = require("browser-sync").create();

gulp.task("watch", function(){
    browserSync.init({
        notify: false,
        server:{
            baseDir: "app"
        }
    });
    watch("./app/index.html", gulp.series('build', 'reloadBrowser'));
    
    watch("./app/assets/styles/**/*.css", gulp.series('css', 'cssInject', 'build'));
    
    watch("./app/assets/scripts/**/*.js", gulp.series('scripts', 'build', 'reloadBrowser'));
});

gulp.task("cssInject", function(){
    return gulp.src("./app/css/style.css")
    .pipe(browserSync.stream());
});

gulp.task("reloadBrowser", function(){
   browserSync.reload();  
});
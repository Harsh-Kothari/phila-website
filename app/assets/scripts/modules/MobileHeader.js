import $ from 'jquery';

class MobileHeader{
    constructor(){
        this.header = $(".header");
        this.menuIcon = $(".header__menu-icon");
        this.menuContainer = $(".header__menu-container");
        this.events();
    }
    events(){
        this.menuIcon.click(this.toggleMobileHeader.bind(this));
    }
    
    toggleMobileHeader(){
        this.menuContainer.toggleClass("header__menu-container__is-visible");
        this.menuIcon.toggleClass("header__menu-icon__close");
        this.header.toggleClass("header__is-expanded");
    }
}

export default MobileHeader;

/*
this.menuIcon.click(this.toggleMobileHeader.bind(this));

in the above line the bind method is used since if the object u=is going through the event the reference of the invoking object changes accordingle as we wanted the reference of this class then we needed to bnd the object of that class so that it completes the activity successfully and JS has the power which can change the reference of the this accordingly as required through the bind keyword
*/